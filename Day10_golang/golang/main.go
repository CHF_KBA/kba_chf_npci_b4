package main

import "fmt"

func main(){
	fmt.Println("Hello NPCI")

//Variables


	var v1,v2 int=10,20
	sum:=v1+v2
	fmt.Println("Sum is",sum)

//input-output
var Name string
fmt.Println("Enter your Name")
fmt.Scan(&Name)
fmt.Println("Entered Name:",Name)

//Looping
sum1:=0
for i:=1;i<=20;i++{
	sum1=sum1+i
}
fmt.Println("Sum from 1st for",sum1)

sum2:=0
i:=1
   for i<10{
   sum2+= i
   i++
   }
   fmt.Println("Sum from 2nd for",sum2)


//    sum3:=1
//    for {
//    sum3+=10
//    }
//    fmt.Println("Sum from 1st for",sum3)


   //Conditional
   it:=10
   if it%2==0{
	fmt.Println("Hello if")
}
if num:=34;num <100 {
	fmt.Println("Hello  if Special")
}
//Function
fmt.Println("Function Execution",multiply(4, 3))
a, b := swap("hello", "world")
	fmt.Println("SWAPPING",a,b)


//Structure
type  Employee struct{
	Name string
	Id int
	Email string
	Department string
	}
 u1:= Employee{"Alice",2006,"alice@gmail.com","Fabric"}
fmt.Println("Employee Details",u1)
fmt.Println("Employee Name",u1.Name)

//pointer-structure
pu:=&u1
fmt.Println("Employee Details with pointer@@@@@@@@@@",*pu)
fmt.Println("Employee Email with pointer@@@@@@@",*&pu.Email)


//Method-Function

vf := Vertex{20,50}
	fmt.Println(adds1(vf))
vm:= Vertex{100,500}
	fmt.Println(vm.adds2())
//Pointer
var ptr1 *int
   ptr2:=222
   ptr1= &ptr2
   fmt.Println("Pointer1",*ptr1)
   *ptr1=*ptr1+200
   fmt.Println("Sum with pointers ",*ptr1)

//pointer With Function
var aa *int
   a1:=2000
   aa= &a1
   var bb *int
   b1:=5000
   bb= &b1
   fmt.Println("Sum with pointer-function:",addp(aa,bb))


}
//Function
func multiply(x int, y int) int {
	return x * y
}
func swap(x, y string) (string, string) {
	return y, x
}

//Method-Function
type Vertex struct {
	a1, b1 int
}
func adds1(vf Vertex) int{
	return(vf.a1 + vf.b1)
}
func (vm Vertex) adds2() int {
	return(vm.a1 + vm.b1)
}
//pointer With Function
func addp(c,d *int) int {
	res:=*c+*d
	return res
} 