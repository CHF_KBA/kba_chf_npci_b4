package main

import "fmt"
v10:=2321
var v1,v2 int//global or package level-Declaration

func main(){

	fmt.Println("v1",v1)
	fmt.Println("v2",v2)

	var v3 bool//local or function level-Declaration
	fmt.Println("v3",v3)

	var v4 bool=true//Declaration+Initialization
	fmt.Println("v4",v4)

	var v5=38.4//Declaration+Initialization
	fmt.Println("v5",v5)

	v6:=34//Declaration+Initialization
	fmt.Println("v6",v6)
	fmt.Printf("variable of type: %T \n",v6)

	v7,v8,v9:="Hello",2.34,23
	fmt.Println("v7",v7)
	fmt.Printf("variable of type: %T \n",v7)
	fmt.Println("v8",v8)
	fmt.Printf("variable of type: %T \n",v8)
	fmt.Println("v9",v9)
	fmt.Printf("variable of type: %T \n",v9)
	fmt.Println("v10",v10)
	fmt.Printf("variable of type: %T \n",v10)

}