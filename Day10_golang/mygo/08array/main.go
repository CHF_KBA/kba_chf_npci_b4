package main

import "fmt"
 
func main()  {
	fmt.Println("Array in golang")
	var arr1 [6]string
	arr1[0]="Hyperledger Project"
	arr1[3]="Fabric"
	fmt.Println("Array length arr1",len(arr1))
	fmt.Println("Array element arr1",arr1)
	var arr2= [5]string{"Indy","Iroha"," "," ","Besu"}
	fmt.Println("Array length arr2",len(arr2))
	fmt.Println("Array elements arr2",arr2)
	fmt.Println("Array elements arr2",arr2[1])

     fmt.Println("Slice in golang")
	 var arr2= []string{"Indy","Iroha","Burrow","Cacti","Besu"}
	 fmt.Println("Slice length arr1",len(arr2))
	 fmt.Println("Slice element arr1",arr2)
	arr2=append(arr2,"Caliper","Explorer" )
	 fmt.Println("Slice length arr1",len(arr2))
	 arr2=append(arr2[0:])
	 fmt.Println("Slice element arr1",arr2)
	 arr2=append(arr2[:6])
	 fmt.Println("Slice element arr1",arr2)
	 arr2=append(arr2[4:6])
	 fmt.Println("Slice element arr1",arr2)

	 //Array with Slice
	 var arr3= [5]string{"Indy","Iroha","Burrow","Cacti","Besu"}
	 arr4:=append(arr3[2:5])
	 fmt.Println("Slice from Array",arr4)
}