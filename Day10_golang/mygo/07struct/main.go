package main

import "fmt"

func main() {
	fmt.Println("Struct in Golang")
	type Employee struct {
		Name       string
		Id         int
		Email      string
		Department string
	}
	u1 := Employee{"Alice", 2006, "alice@gmail.com", "Fabric"}
	fmt.Println("Employee Details", u1)
	fmt.Println("Employee Name", u1.Name)
	u2 := Employee{}
	fmt.Println("Employee Details", u2)
	fmt.Println("Employee Details", u2.Department)
	//Struct with pointer
	p := &u1
	fmt.Println("Employee Details with pointer", *p)
	fmt.Println("Employee Email with", *&p.Email)
}
