package main
import
(
	"fmt"
)
func main(){
	fmt.Println("Loop")
	
	sum := 0
//TYPE1 for
	for i := 0; i < 10; i++ {
		sum += i
	}

//TYPE2 for
	i:=1
	for i<10{
	sum += i
	if i%2==0{
		fmt.Println("Hello continue")
		continue
	}
	if i%4==0 {
		fmt.Println("Hello break")
		break
	}
	if i%3==0 {
		
		fmt.Println("iiiii",i)
		fmt.Println("Hello goto")
		goto goo
	}
	i++
	}
	goo:fmt.Println("Hello goto label")
	fmt.Println(sum)
//TYPE 2 if
	if i%2==0{
		fmt.Println("Hello if")
	}else if i>4 {
		fmt.Println("Hello if")
	
	}else {

		fmt.Println("Hello else")
	}
//TYPE 3 if
	if(i==2){
		fmt.Println("Hello if")
	}else{
		fmt.Println("Hello else")
	}
//TYPE 4 If
	if num:=34;num <100 {
		fmt.Println("Hello Type 4 iffff")
	}
//Type:switch
	var name string
	fmt.Println("Enter Your Name")
	fmt.Scan(&name)
	switch name {
	case "anu":
		fmt.Println("Hello anu")
	case "minu":
		fmt.Println("Hello minu")
	fallthrough//works as in absence of break in normal flow
	case "vinu":
		fmt.Println("Hello vinu")	
}
}
