package main

import "fmt"

  func add(x, y int) int {
	return x + y
}
func swap(x, y string) (string, string) {
	return y, x
}

func main() {
	a, b := swap("hello", "world")
	 defer fmt.Println(a, b)
	 fmt.Println(add(42, 13))
	const t=false
	defer fmt.Println("tttttttt", t)
}

