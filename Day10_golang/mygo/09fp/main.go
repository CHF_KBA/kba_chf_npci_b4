package main

import "fmt"
  
type Vertex struct {
	a1, b1 int
}

// func (v Vertex) add() int {
// 	return (v.a1 + v.b1)
// }

// func main() {
// 	v := Vertex{20,50}
// 	fmt.Println(v.add())
// }

func add (v *Vertex)  int {
	return (v.a1 + v.b1)
}

func main() {
	v := Vertex{20,50}
	fmt.Println(add(&v))
}
