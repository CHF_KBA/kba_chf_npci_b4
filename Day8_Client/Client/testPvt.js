const {clientApplication}=require('./client')
let dealerClient=new clientApplication()
const transientData={
    make:Buffer.from('Hatchback'),
    model:Buffer.from('Nexon'),
    color:Buffer.from('White'),
    dealerName:Buffer.from('Dealer1')
}
dealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",//User1
    "autochannel",
    "KBA-Automobile",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "Order-01"
   ).then(message => {
    console.log(message.toString())})