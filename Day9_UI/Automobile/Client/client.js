const { profile } = require('./profile')
const { Wallets, Gateway } = require('fabric-network')
const path = require('path')
const fs = require('fs')

class clientApplication {
    async generateAndSubmitTxn(role, identityLabel, channelName, chaincodeName, contractName, txnType, transientData, txnName, ...args) {
        let gateway = new Gateway()
        try {
            this.Profile = profile[role.toLowerCase()]
            const ccpPath = path.resolve(this.Profile["CP"])
            const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf-8'))
            let wallet = await Wallets.newFileSystemWallet(this.Profile["Wallet"])
            await gateway.connect(ccp, { wallet, identity: identityLabel, discovery: { enabled: true, asLocalhost: true } })
            let channel = await gateway.getNetwork(channelName)
            let contract = await channel.getContract(chaincodeName, contractName)
            let result
            if (txnType == "invokeTxn") {
                result = await contract.submitTransaction(txnName, ...args)
            } else if (txnType == "queryTxn") {
                result = await contract.evaluateTransaction(txnName, ...args)
            } else {
                result = await contract.createTransaction(txnName).setTransient(transientData).submit(...args)
            }
            // return result          
            return Promise.resolve(result)
        } catch (error) {
            console.log("Error Occured", error)
            return Promise.reject(error);
        } finally {
            console.log("Disconnect from Gateway")
            gateway.disconnect()
        }
    }

}
module.exports = { clientApplication }